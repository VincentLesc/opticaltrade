/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you require will output into a single css file (app.scss in this case)
require('../css/app.scss');
require('../css/responsive.scss');
require('../css/ui.scss');
require('../css/_variables-custom.scss');

// Need jQuery? Install it with "yarn add jquery", then uncomment to require it.
const $ = require('jquery');

import 'bootstrap'
import '@fortawesome/fontawesome-free/js/all.min'
import './partner.create_account'

$(function () {
    $('[data-toggle="tooltip"]').tooltip()
})