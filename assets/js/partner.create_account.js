window.onload = function () {
    document.querySelector('#cta-section-2').addEventListener('click', function () {
        document.getElementById("section-2").scrollIntoView({block: "start", inline: "nearest", behavior: "smooth"});
    });
    document.querySelector('#cta-section-3').addEventListener('click', function () {
        document.getElementById("section-form").scrollIntoView({block: "start", inline: "nearest", behavior: "smooth"});
    })
}