import './utils/app.utils';
import Notifications from 'awesome-notifications';
import axios from "axios";

window.onload = function() {
    document.querySelector('#add-employee').addEventListener('click', function () {
        employeeFormModal();
    });
    document.querySelectorAll('[id^="edit-user-"]').forEach(function (userEdit) {
        userEdit.addEventListener('click', function () {
            let userId = this.dataset.uid;
            employeeFormModal(userId);
        })
    });
};

function employeeFormModal(userId) {
    let modal = document.querySelector('#global-modal');
    axios.post(
        '/partner/account/employee/form/api',
        {
            'uid': userId
        }
    ).then(
        function(response) {
            modal.style.display = 'block';
            modal.innerHTML = response.data;
            let modalContent = document.querySelector('#add-employee-modal');
            $(modalContent).modal({show: true});
            let employeeType = document.querySelector('#submit-employee-type');
            employeeType.addEventListener('click', function () {
                submitEmployeeType(userId);
            });
            let $disableEmployee =document.querySelector('#disable-user');
            $disableEmployee.addEventListener('click', function () {
                disableEmployee(userId)
            });
        }
    )
}

function submitEmployeeType(userId) {
    let email = document.querySelector('#employee_email').value;
    let roles = [];
    document.querySelectorAll('[id^="employee_roles_"]').forEach(function (input) {
        if (input.checked === true) {
            roles.push(input.value)
        }
    });
    if (userId !== undefined) {
        axios.post(
            '/partner/account/employee/edit/api',
            {
                'email': email,
                'roles': roles,
                'userId': userId
            }
        ).then(
            function (response) {
                if (response.data === 'done') {
                    let modalContent = document.querySelector('#close');
                    modalContent.click();
                    let option = {
                        'position': 'top-right'
                    };
                }
                document.location.reload();
            }
        ).catch(
            function (error) {
                let $alert = document.createElement('div');
                $alert.classList.add('alert');
                $alert.classList.add('alert-danger');
                $alert.innerHTML = error.response.data.title;
                document.querySelector('.modal-body').prepend($alert)
            }
        )

    } else {
        axios.post(
            '/partner/account/employee/add/api',
            {
                'email': email,
                'roles': roles,
            }
        ).then(
            function (response) {
                if (response.data === 'done') {
                    let modalContent = document.querySelector('#close');
                    modalContent.click();
                    let option = {
                        'position': 'top-right'
                    };
                }
                document.location.reload();
            }
        ).catch(
            function (error) {
                let $alert = document.createElement('div');
                $alert.classList.add('alert');
                $alert.classList.add('alert-danger');
                $alert.innerHTML = error.response.data.title;
                document.querySelector('.modal-body').prepend($alert)
            }
        )
    }
}

function disableEmployee(userId) {
    axios.post(
        '/partner/account/employee/disable',
        {
            'uid' : userId
        }
    ).then(
        function (response) {
            let modalContent = document.querySelector('#add-employee-modal');
            $(modalContent).modal({show: false});
            document.location.reload();
        }
    )
}
