<?php
namespace App\Event\PartnerEvent;

use App\Entity\Partner\PartnerCompany;
use Symfony\Contracts\EventDispatcher\Event;

class PartnerCreatedEvent extends Event
{
    public const PARTNER_CREATED = 'partner.created';

    protected $partnerCompany;

    public function __construct(PartnerCompany $partnerCompany)
    {
        $this->partnerCompany = $partnerCompany;
    }

    public function getPartnerCompany()
    {
        return $this->partnerCompany;
    }
}