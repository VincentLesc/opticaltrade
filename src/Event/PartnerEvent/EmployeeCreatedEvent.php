<?php
namespace App\Event\PartnerEvent;

use App\Entity\Partner\PartnerCompany;
use App\Entity\User;
use Symfony\Contracts\EventDispatcher\Event;

class EmployeeCreatedEvent extends Event
{
    public const EMPLOYEE_CREATED = 'employee.created';

    protected $employee;

    public function __construct(User $employee)
    {
        $this->employee = $employee;
    }

    public function getEmployee()
    {
        return $this->employee;
    }
}
