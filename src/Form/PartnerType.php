<?php

namespace App\Form;

use App\Entity\Partner\PartnerCompany;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PartnerType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('legalRepresentativeCivility', ChoiceType::class, [
                'choices' => [
                    'Monsieur' => 'Monsieur',
                    'Madame' => 'Madame'
                ]
            ])
            ->add('legalRepresentativeFirstname', TextType::class)
            ->add('legalRepresentativeLastname')
            ->add('name')
            ->add('legalAddress')
            ->add('legalZipCode')
            ->add('legalTown')
            ->add('country')
            ->add('phone')
            ->add('SIRET')
            ->add('TVAId')
            ->add('IBAN')
            ->add('saveAfterRegistration', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => PartnerCompany::class,
        ]);
    }
}
