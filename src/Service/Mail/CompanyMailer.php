<?php

namespace App\Service\Mail;

class CompanyMailer
{
    private $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function confirmedAccount($user)
    {
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('send@example.com')
            ->setTo($user->getEmail())
            ->setBody(
                '<table cellspacing="0" cellpadding="0" border="0" width="100%">

<tr>

<td style="background-color: #0000cc">

test

</td>

</tr>

</table>',
                'text/html'
            );

        $this->mailer->send($message);
    }
}
