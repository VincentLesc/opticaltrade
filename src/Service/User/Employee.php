<?php


namespace App\Service\User;


use App\Entity\Partner\PartnerCompany;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class Employee
{
    protected $passwordEncoderInterface;

    protected $security;

    public function __construct(UserPasswordEncoderInterface $passwordEncoderInterface, UserSecurity $security)
    {
        $this->passwordEncoderInterface = $passwordEncoderInterface;
        $this->security = $security;
    }

    public function createEmployee(string $mail, PartnerCompany $company): User
    {
        $user = new User();
        $user->setEmail($mail)
            ->setPassword($this->security->getRandomEncodedPassword($user))
            ->setPasswordToken($this->security->getRandomToken())
            ->setPasswordTokenCreatedAt(new \DateTime('now'))
            ->setPartnerCompany($company)
            ->setIsActiveEmployee(true);

        return $user;
    }

}
