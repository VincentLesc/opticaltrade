<?php


namespace App\Service\User;


use App\Entity\Partner\PartnerCompany;
use App\Entity\User;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserSecurity
{
    protected $passwordEncoderInterface;

    protected $mailer;

    public function __construct(UserPasswordEncoderInterface $passwordEncoderInterface, \Swift_Mailer $mailer)
    {
        $this->passwordEncoderInterface = $passwordEncoderInterface;
        $this->mailer = $mailer;
    }


    public function getRandomEncodedPassword($user)
    {

        return $this->passwordEncoderInterface->encodePassword(
            $user,
            uniqid()
        );
    }

    public function getRandomToken()
    {
        return uniqid('AZE_');
    }

    public function sendMailWithPasswordResetToken(User $user)
    {
        //ToDO = set sender
        //TODO = Mail template
        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('send@example.com')
            ->setTo($user->getEmail())
            ->setBody(
                '<table cellspacing="0" cellpadding="0" border="0" width="100%">

<tr>

<td style="background-color: #0000cc">

test

</td>

</tr>

</table>',
                'text/html'
            );

        $this->mailer->send($message);
    }
}
