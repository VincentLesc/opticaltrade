<?php


namespace App\Service\Admin;


use App\Entity\Competitive\SiteBrand;
use App\Entity\Competitive\SiteProductPage;
use App\Entity\Product\Product;
use App\Entity\Product\ProductPicture;
use App\Repository\Competitive\SiteProductPageRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DomCrawler\Crawler;

class CompetitiveScan
{
    private $em;

    private $productPageRepository;

    public function __construct(ObjectManager $em, SiteProductPageRepository $productPageRepository)
    {
        $this->em = $em;
        $this->productPageRepository = $productPageRepository;
    }

    public function crawlerBrandIndex(string $url, SiteBrand $brand)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($this->getHTML($url));

        $result = $crawler->filterXPath('//a[contains(@class, "eoViewsListviewItemClassicInner sContent")]')->evaluate('@title');
        foreach ($result as $domElement) {
            dump($domElement->nodeValue);
            $page = new SiteProductPage();
            $title = $domElement->parentNode->attributes->getNamedItem('title')->nodeValue;
            $url = $domElement->parentNode->attributes->getNamedItem('href')->nodeValue;
            if (!$this->productPageRepository->findOneBy(['title'=> $title])) {
                $page->setUrl($url)
                    ->setSiteBrand($brand)
                    ->setTitle($title);
                $this->em->persist($page);
                $this->em->flush();
            }
        }
        return 'done';
    }

    public function crawlerProductPage(string $url, SiteProductPage $productPage)
    {
        $crawler = new Crawler();
        $crawler->addHtmlContent($this->getHTML($url));
        $ean = $crawler->filterXPath('//meta[contains(@itemprop, "gtin8")]')->evaluate('@content')->getNode(0)->nodeValue;
        $name = $crawler->filterXPath('//span[contains(@itemprop, "name")]')->getNode(0)->nodeValue;
        $price = $crawler->filterXPath('//span[contains(@itemprop, "price")]')->evaluate('@content')->getNode(0)->nodeValue;
        $priceCurrency = $crawler->filterXPath('//meta[contains(@itemprop, "priceCurrency")]')->evaluate('@content')->getNode(0)->nodeValue;
        $image = $crawler->filterXPath('//img[contains(@class, "eoDetailImage")]')->evaluate('@src')->getNode(0)->nodeValue;
        $details = $crawler->filterXPath('//table[contains(@class, "eoDetailPropertyListTable")]')->evaluate('tr');
        foreach ($details as $detail) {
            for ($i =0 ; $i < $detail->childNodes->length; $i++) {
                $property = $detail->childNodes->item($i)->nodeValue;
                if ($property === "Dimension des verres") {
                    $productPage->setSizeA($detail->childNodes->item($i+2)->nodeValue);
                    $this->isMultipleSize($productPage->getSizeA(), $productPage);
                } elseif ($property === "Dimension du pont nasal") {
                    $productPage->setSizeD($detail->childNodes->item($i+2)->nodeValue);
                    $this->isMultipleSize($productPage->getSizeD(), $productPage);
                } elseif ($property === "Longueur des branches") {
                    $productPage->setSizeTemple($detail->childNodes->item($i+2)->nodeValue);
                    $this->isMultipleSize($productPage->getSizeTemple(), $productPage);
                } elseif ($property === "Couleur de monture") {
                    $productPage->setFrameColor($detail->childNodes->item($i+1)->nodeValue);
                } elseif ($property === "Couleur de verre") {
                    $productPage->setGlassesColor($detail->childNodes->item($i+1)->nodeValue);
                }
            }
        }


        if ($productPage->getPicturesLength() === 0) {
            $newFile = $this->crawlerGetPicture($image, $ean);
            $productPicture = new ProductPicture();
            $productPicture->setFilename($newFile);
            $this->em->persist($productPicture);
            $productPage->addProductPicture($productPicture);
        }

        $productPage->setEan13($ean)
            ->setName($name)
            ->setPrice($price)
            ->setPriceCurrency($priceCurrency);


        $this->em->persist($productPage);

        $this->em->flush();

        return $productPage;
    }

    public function crawlerGetPicture($url, $ean13)
    {
        $oldImage = file_get_contents('https:' . $url);
        $newFile = 'uploads/content/product/'.$ean13.'.jpeg';
        file_put_contents($newFile, $oldImage);

        return $newFile;
    }


    public function isMultipleSize(string $property, SiteProductPage $productPage)
    {
        if ($productPage->getIsToBeReviewed())
            return true;
        $test = explode('/',$property);
        return count($test) === 1;
    }

    public function transferToProduct(SiteProductPage $item)
    {
        $product = new Product();
        $product
            ->addSiteProductPage($item)
            ->setBrand($item->getOfficialProductBrand())
            ->setEAN13($item->getEan13())
            ->setReference($item->getName())
            ->setGlassesColor($item->getGlassesColor())
            ->setFrameColor($item->getFrameColor())
            ->setSizeTemple($item->getSizeTemple())
            ->setSizeA($item->getSizeA())
            ->setSizeD($item->getSizeD())
            ->setGlassesOption($item->getGlassesOption())
            ->setPrices([$item->getPrice()]);
        foreach ($item->getProductPictures() as $picture) {
            $product->addPicture($picture);
        }
        $this->em->persist($product);
        $this->em->flush();
    }

    public function getHTML(string $url)
    {
        return "<<<'HTML'".file_get_contents($url)."HTML";
    }
}
