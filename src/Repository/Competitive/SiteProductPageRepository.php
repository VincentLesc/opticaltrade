<?php

namespace App\Repository\Competitive;

use App\Entity\Competitive\SiteProductPage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SiteProductPage|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteProductPage|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteProductPage[]    findAll()
 * @method SiteProductPage[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteProductPageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteProductPage::class);
    }

    // /**
    //  * @return SiteProductPage[] Returns an array of SiteProductPage objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteProductPage
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
