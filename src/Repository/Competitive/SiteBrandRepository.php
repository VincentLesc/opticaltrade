<?php

namespace App\Repository\Competitive;

use App\Entity\Competitive\SiteBrand;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method SiteBrand|null find($id, $lockMode = null, $lockVersion = null)
 * @method SiteBrand|null findOneBy(array $criteria, array $orderBy = null)
 * @method SiteBrand[]    findAll()
 * @method SiteBrand[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SiteBrandRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SiteBrand::class);
    }

    // /**
    //  * @return SiteBrand[] Returns an array of SiteBrand objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?SiteBrand
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
