<?php

namespace App\Repository\Content\Component;

use App\Entity\Content\Component\Slider;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Slider|null find($id, $lockMode = null, $lockVersion = null)
 * @method Slider|null findOneBy(array $criteria, array $orderBy = null)
 * @method Slider[]    findAll()
 * @method Slider[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SliderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Slider::class);
    }

    public function findActiveSlidePerPage($page)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.publishStartAt <= :start')
            ->setParameter('start', new \DateTime('now'))
            ->andWhere('s.publishEndAt >= :end')
            ->setParameter('end', new \DateTime('now'))
            ->andWhere('s.page = :page')
            ->setParameter('page', $page)
            ->orderBy('s.id', 'DESC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Slider[] Returns an array of Slider objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('s.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Slider
    {
        return $this->createQueryBuilder('s')
            ->andWhere('s.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
