<?php

namespace App\Repository\Partner;

use App\Entity\Partner\PartnerProductForSale;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method PartnerProductForSale|null find($id, $lockMode = null, $lockVersion = null)
 * @method PartnerProductForSale|null findOneBy(array $criteria, array $orderBy = null)
 * @method PartnerProductForSale[]    findAll()
 * @method PartnerProductForSale[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PartnerProductForSaleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PartnerProductForSale::class);
    }

    // /**
    //  * @return PartnerProductForSale[] Returns an array of PartnerProductForSale objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PartnerProductForSale
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
