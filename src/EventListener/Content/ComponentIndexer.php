<?php
namespace App\EventListener\Content;

use App\Entity\Content\Component\Slider;
use Doctrine\ORM\Event\LifecycleEventArgs;

class ComponentIndexer
{
    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Slider) {
            $entity->setUpdatedAt(new \DateTime('now'));
        }
    }

    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Slider) {
            $entity->setUpdatedAt(new \DateTime('now'));
        }
    }
}