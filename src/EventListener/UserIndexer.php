<?php

namespace App\EventListener;

use App\Entity\User;
use App\Event\PartnerEvent\EmployeeCreatedEvent;
use Doctrine\ORM\Event\LifecycleEventArgs;

class UserIndexer
{
    protected $mailer;

    public function __construct(\Swift_Mailer $mailer)
    {
        $this->mailer = $mailer;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof User) {
            $entity->setCreatedAt(new \DateTime('now'));
        }
    }

    public function onEmployeeCreated(EmployeeCreatedEvent $event)
    {
        /**@var User $employee */
        $employee = $event->getEmployee();

        //ToDO = set sender


        $message = (new \Swift_Message('Hello Email'))
            ->setFrom('send@example.com')
            ->setTo($employee->getEmail())
            ->setBody(
                '<div>Hello totot</div>',
                'text/html'
            );

        $this->mailer->send($message);
    }
}
