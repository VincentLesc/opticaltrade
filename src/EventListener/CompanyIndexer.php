<?php
namespace App\EventListener;

use App\Entity\Partner\PartnerCompany;
use App\Entity\User;
use App\Event\PartnerEvent\PartnerCreatedEvent;
use App\Repository\UserRepository;
use App\Service\Mail\CompanyMailer;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreUpdateEventArgs;
use Symfony\Component\Security\Core\Security;

class CompanyIndexer
{
    private $security;

    private $userRepository;

    private $mailer;

    public function __construct(Security $security, UserRepository $userRepository, CompanyMailer $mailer)
    {
            $this->security = $security;
            $this->userRepository = $userRepository;
            $this->mailer = $mailer;
    }

    public function prePersist(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof PartnerCompany) {
            $entity->setIsConfirm(false);
        }
    }

    public function preUpdate(PreUpdateEventArgs $eventArgs)
    {
        $entity = $eventArgs->getObject();
        if ($entity instanceof PartnerCompany) {
            if ($eventArgs->hasChangedField('isConfirm')
                && $eventArgs->getNewValue('isConfirm') === true
            ) {
                /**@var User $user */
                $user = $this->security->getUser();
                $entity->setConfirmedAt(new \DateTime('now'))
                    ->setConfirmedBy($user);
                $this->mailer->confirmedAccount($entity->getMainUser());

            }
        }
    }

    public function onPartnerCreated(PartnerCreatedEvent $event)
    {
        dump($event->getPartnerCompany());
    }


}
