<?php

namespace App\Entity\Partner;

use App\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Partner\PartnerCompanyRepository")
 */
class PartnerCompany
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $legalAddress;

    /**
     * @ORM\Column(type="integer")
     */
    private $legalZipCode;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $legalTown;

    /**
     * @ORM\Column(type="integer")
     */
    private $SIRET;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\User", mappedBy="partnerCompany")
     */
    private $user;

    /**
     * @ORM\Column(type="string", length=16)
     */
    private $TVAId;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $IBAN;

    /**
     * @ORM\Column(type="boolean")
     */
    private $isConfirm;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     */
    private $confirmedBy;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    private $confirmedAt;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $legalRepresentativeFirstname;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $legalRepresentativeLastname;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $legalRepresentativeCivility;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $phone;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $country;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\User", cascade={"persist", "remove"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $mainUser;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Partner\PartnerProductForSale", mappedBy="partnerCompany")
     */
    private $partnerProductForSales;

    public function __construct()
    {
        $this->user = new ArrayCollection();
        $this->partnerProductForSales = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getLegalAddress(): ?string
    {
        return $this->legalAddress;
    }

    public function setLegalAddress(string $legalAddress): self
    {
        $this->legalAddress = $legalAddress;

        return $this;
    }

    public function getLegalZipCode(): ?int
    {
        return $this->legalZipCode;
    }

    public function setLegalZipCode(int $legalZipCode): self
    {
        $this->legalZipCode = $legalZipCode;

        return $this;
    }

    public function getLegalTown(): ?string
    {
        return $this->legalTown;
    }

    public function setLegalTown(string $legalTown): self
    {
        $this->legalTown = $legalTown;

        return $this;
    }

    public function getSIRET(): ?int
    {
        return $this->SIRET;
    }

    public function setSIRET(int $SIRET): self
    {
        $this->SIRET = $SIRET;

        return $this;
    }

    /**
     * @return Collection|User[]
     */
    public function getUser(): Collection
    {
        return $this->user;
    }

    public function addUser(User $user): self
    {
        if (!$this->user->contains($user)) {
            $this->user[] = $user;
            $user->setPartnerCompany($this);
        }

        return $this;
    }

    public function removeUser(User $user): self
    {
        if ($this->user->contains($user)) {
            $this->user->removeElement($user);
            // set the owning side to null (unless already changed)
            if ($user->getPartnerCompany() === $this) {
                $user->setPartnerCompany(null);
            }
        }

        return $this;
    }

    public function getTVAId(): ?string
    {
        return $this->TVAId;
    }

    public function setTVAId(string $TVAId): self
    {
        $this->TVAId = $TVAId;

        return $this;
    }

    public function getIBAN(): ?string
    {
        return $this->IBAN;
    }

    public function setIBAN(string $IBAN): self
    {
        $this->IBAN = $IBAN;

        return $this;
    }

    public function getIsConfirm(): ?bool
    {
        return $this->isConfirm;
    }

    public function setIsConfirm(bool $isConfirm): self
    {
        $this->isConfirm = $isConfirm;

        return $this;
    }

    public function getConfirmedBy(): ?User
    {
        return $this->confirmedBy;
    }

    public function setConfirmedBy(?User $confirmedBy): self
    {
        $this->confirmedBy = $confirmedBy;

        return $this;
    }

    public function getConfirmedAt(): ?\DateTimeInterface
    {
        return $this->confirmedAt;
    }

    public function setConfirmedAt(?\DateTimeInterface $confirmedAt): self
    {
        $this->confirmedAt = $confirmedAt;

        return $this;
    }

    public function getLegalRepresentativeFirstname(): ?string
    {
        return $this->legalRepresentativeFirstname;
    }

    public function setLegalRepresentativeFirstname(string $legalRepresentativeFirstname): self
    {
        $this->legalRepresentativeFirstname = $legalRepresentativeFirstname;

        return $this;
    }

    public function getLegalRepresentativeLastname(): ?string
    {
        return $this->legalRepresentativeLastname;
    }

    public function setLegalRepresentativeLastname(string $legalRepresentativeLastname): self
    {
        $this->legalRepresentativeLastname = $legalRepresentativeLastname;

        return $this;
    }

    public function getLegalRepresentativeCivility(): ?string
    {
        return $this->legalRepresentativeCivility;
    }

    public function setLegalRepresentativeCivility(?string $legalRepresentativeCivility): self
    {
        $this->legalRepresentativeCivility = $legalRepresentativeCivility;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getCountry(): ?string
    {
        return $this->country;
    }

    public function setCountry(string $country): self
    {
        $this->country = $country;

        return $this;
    }

    public function getMainUser(): ?User
    {
        return $this->mainUser;
    }

    public function setMainUser(User $mainUser): self
    {
        $this->mainUser = $mainUser;

        return $this;
    }

    /**
     * @return Collection|PartnerProductForSale[]
     */
    public function getPartnerProductForSales(): ?Collection
    {
        return $this->partnerProductForSales;
    }

    public function addPartnerProductForSale(PartnerProductForSale $partnerProductForSale): self
    {
        if (!$this->partnerProductForSales->contains($partnerProductForSale)) {
            $this->partnerProductForSales[] = $partnerProductForSale;
            $partnerProductForSale->setPartnerCompany($this);
        }

        return $this;
    }

    public function removePartnerProductForSale(PartnerProductForSale $partnerProductForSale): self
    {
        if ($this->partnerProductForSales->contains($partnerProductForSale)) {
            $this->partnerProductForSales->removeElement($partnerProductForSale);
            // set the owning side to null (unless already changed)
            if ($partnerProductForSale->getPartnerCompany() === $this) {
                $partnerProductForSale->setPartnerCompany(null);
            }
        }

        return $this;
    }
}
