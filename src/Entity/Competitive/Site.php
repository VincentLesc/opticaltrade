<?php

namespace App\Entity\Competitive;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Competitive\SiteRepository")
 */
class Site
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $base_url;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Competitive\SiteBrand", mappedBy="site",cascade={"persist"})
     */
    private $siteBrands;

    public function __construct()
    {
        $this->siteBrands = new ArrayCollection();
    }

    public function __toString(): string
    {
        return $this->name;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBaseUrl(): ?string
    {
        return $this->base_url;
    }

    public function setBaseUrl(string $base_url): self
    {
        $this->base_url = $base_url;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|SiteBrand[]
     */
    public function getSiteBrands(): Collection
    {
        return $this->siteBrands;
    }

    public function addSiteBrand(SiteBrand $siteBrand): self
    {
        if (!$this->siteBrands->contains($siteBrand)) {
            $this->siteBrands[] = $siteBrand;
            $siteBrand->setSite($this);
        }

        return $this;
    }

    public function removeSiteBrand(SiteBrand $siteBrand): self
    {
        if ($this->siteBrands->contains($siteBrand)) {
            $this->siteBrands->removeElement($siteBrand);
            // set the owning side to null (unless already changed)
            if ($siteBrand->getSite() === $this) {
                $siteBrand->setSite(null);
            }
        }

        return $this;
    }
}
