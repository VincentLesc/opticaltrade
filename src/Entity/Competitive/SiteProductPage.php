<?php

namespace App\Entity\Competitive;

use App\Entity\Product\Brand;
use App\Entity\Product\Product;
use App\Entity\Product\ProductPicture;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Competitive\SiteProductPageRepository")
 */
class SiteProductPage
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $ean13;

    /**
     * @ORM\Column(type="string", length=128)
     */
    private $title;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Competitive\SiteBrand", inversedBy="pages")
     */
    private $siteBrand;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $priceCurrency;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $sizeA;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $sizeD;

    /**
     * @ORM\Column(type="string", length=8, nullable=true)
     */
    private $sizeTemple;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $glassesColor;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $frameColor;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $glassesOption;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product\ProductPicture", mappedBy="competitiveProduct")
     */
    private $productPictures;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Product", inversedBy="siteProductPages")
     */
    private $relatedProduct;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $isToBeReviewed;

    public function __construct()
    {
        $this->productPictures = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getEan13(): ?string
    {
        return $this->ean13;
    }

    public function setEan13(?string $ean13): self
    {
        $this->ean13 = $ean13;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getSiteBrand(): ?SiteBrand
    {
        return $this->siteBrand;
    }

    public function getOfficialProductBrand(): ?Brand
    {
        return $this->getSiteBrand()->getBrand();
    }

    public function setSiteBrand(?SiteBrand $siteBrand): self
    {
        $this->siteBrand = $siteBrand;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(?string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPriceCurrency(): ?string
    {
        return $this->priceCurrency;
    }

    public function setPriceCurrency(?string $priceCurrency): self
    {
        $this->priceCurrency = $priceCurrency;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(?string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSizeA(): ?string
    {
        return $this->sizeA;
    }

    public function setSizeA(?string $sizeA): self
    {
        $this->sizeA = $sizeA;

        return $this;
    }

    public function getSizeD(): ?string
    {
        return $this->sizeD;
    }

    public function setSizeD(?string $sizeD): self
    {
        $this->sizeD = $sizeD;

        return $this;
    }

    public function getSizeTemple(): ?string
    {
        return $this->sizeTemple;
    }

    public function setSizeTemple(?string $sizeTemple): self
    {
        $this->sizeTemple = $sizeTemple;

        return $this;
    }

    public function getGlassesColor(): ?string
    {
        return $this->glassesColor;
    }

    public function setGlassesColor(?string $glassesColor): self
    {
        $this->glassesColor = $glassesColor;

        return $this;
    }

    public function getFrameColor(): ?string
    {
        return $this->frameColor;
    }

    public function setFrameColor(?string $frameColor): self
    {
        $this->frameColor = $frameColor;

        return $this;
    }

    public function getGlassesOption(): ?string
    {
        return $this->glassesOption;
    }

    public function setGlassesOption(?string $glassesOption): self
    {
        $this->glassesOption = $glassesOption;

        return $this;
    }

    /**
     * @return Collection|ProductPicture[]
     */
    public function getProductPictures(): Collection
    {
        return $this->productPictures;
    }

    public function addProductPicture(ProductPicture $productPicture): self
    {
        if (!$this->productPictures->contains($productPicture)) {
            $this->productPictures[] = $productPicture;
            $productPicture->setCompetitiveProduct($this);
        }

        return $this;
    }

    public function removeProductPicture(ProductPicture $productPicture): self
    {
        if ($this->productPictures->contains($productPicture)) {
            $this->productPictures->removeElement($productPicture);
            // set the owning side to null (unless already changed)
            if ($productPicture->getCompetitiveProduct() === $this) {
                $productPicture->setCompetitiveProduct(null);
            }
        }

        return $this;
    }

    public function getPicturesLength()
    {
        return $this->getProductPictures()->count();
    }

    public function getRelatedProduct(): ?Product
    {
        return $this->relatedProduct;
    }

    public function setRelatedProduct(?Product $relatedProduct): self
    {
        $this->relatedProduct = $relatedProduct;

        return $this;
    }

    public function getIsToBeReviewed(): ?bool
    {
        return $this->isToBeReviewed;
    }

    public function setIsToBeReviewed(?bool $isToBeReviewed): self
    {
        $this->isToBeReviewed = $isToBeReviewed;

        return $this;
    }
}
