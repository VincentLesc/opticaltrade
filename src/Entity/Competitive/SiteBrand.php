<?php

namespace App\Entity\Competitive;

use App\Entity\Product\Brand;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Competitive\SiteBrandRepository")
 */
class SiteBrand
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $baseUrl;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Competitive\Site", inversedBy="siteBrands",cascade={"persist"})
     * @ORM\JoinColumn(nullable=false)
     */
    private $site;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Competitive\SiteProductPage", mappedBy="siteBrand")
     */
    private $pages;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Brand", inversedBy="siteBrands" ,cascade={"persist"})
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=32, nullable=true)
     */
    private $title;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime('now'));
        $this->pages = new ArrayCollection();
        $this->createdAt = new \DateTime('now');
    }

    public function __toString(): string
    {
       return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }


    public function getBaseUrl(): ?string
    {
        return $this->baseUrl;
    }

    public function setBaseUrl(string $baseUrl): self
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getSite(): ?Site
    {
        return $this->site;
    }

    public function setSite(?Site $site): self
    {
        $this->site = $site;

        return $this;
    }

    /**
     * @return Collection|SiteProductPage[]
     */
    public function getPages(): Collection
    {
        return $this->pages;
    }

    public function addPage(SiteProductPage $page): self
    {
        if (!$this->pages->contains($page)) {
            $this->pages[] = $page;
            $page->setSiteBrand($this);
        }

        return $this;
    }

    public function removePage(SiteProductPage $page): self
    {
        if ($this->pages->contains($page)) {
            $this->pages->removeElement($page);
            // set the owning side to null (unless already changed)
            if ($page->getSiteBrand() === $this) {
                $page->setSiteBrand(null);
            }
        }

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(?string $title): self
    {
        $this->title = $title;

        return $this;
    }
}
