<?php

namespace App\Entity\Product;

use App\Entity\Competitive\SiteProductPage;
use App\Entity\Partner\PartnerProductForSale;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\ProductRepository")
 */
class Product
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=16, nullable=true)
     */
    private $EAN13;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $reference;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Brand", inversedBy="products")
     * @ORM\JoinColumn(nullable=false)
     */
    private $brand;

    /**
     * @ORM\Column(type="array", nullable=true)
     */
    private $prices = [];

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $sizeA;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $sizeD;

    /**
     * @ORM\Column(type="string", length=8)
     */
    private $sizeTemple;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $frameColor;

    /**
     * @ORM\Column(type="string", length=32)
     */
    private $glassesColor;

    /**
     * @ORM\Column(type="string", length=64, nullable=true)
     */
    private $glassesOption;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product\ProductPicture", mappedBy="product")
     */
    private $picture;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Competitive\SiteProductPage", mappedBy="relatedProduct")
     */
    private $siteProductPages;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Partner\PartnerProductForSale", mappedBy="product")
     */
    private $partnerProductForSales;

    public function __construct()
    {
        $this->picture = new ArrayCollection();
        $this->siteProductPages = new ArrayCollection();
        $this->partnerProductForSales = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->reference;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getEAN13(): ?string
    {
        return $this->EAN13;
    }

    public function setEAN13(?string $EAN13): self
    {
        $this->EAN13 = $EAN13;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getBrand(): ?Brand
    {
        return $this->brand;
    }

    public function setBrand(?Brand $brand): self
    {
        $this->brand = $brand;

        return $this;
    }

    public function getPrices(): ?array
    {
        return $this->prices;
    }

    public function setPrices(?array $prices): self
    {
        $this->prices = $prices;

        return $this;
    }

    public function getSizeA(): ?string
    {
        return $this->sizeA;
    }

    public function setSizeA(string $sizeA): self
    {
        $this->sizeA = $sizeA;

        return $this;
    }

    public function getSizeD(): ?string
    {
        return $this->sizeD;
    }

    public function setSizeD(string $sizeD): self
    {
        $this->sizeD = $sizeD;

        return $this;
    }

    public function getSizeTemple(): ?string
    {
        return $this->sizeTemple;
    }

    public function setSizeTemple(string $sizeTemple): self
    {
        $this->sizeTemple = $sizeTemple;

        return $this;
    }

    public function getFrameColor(): ?string
    {
        return $this->frameColor;
    }

    public function setFrameColor(string $frameColor): self
    {
        $this->frameColor = $frameColor;

        return $this;
    }

    public function getGlassesColor(): ?string
    {
        return $this->glassesColor;
    }

    public function setGlassesColor(string $glassesColor): self
    {
        $this->glassesColor = $glassesColor;

        return $this;
    }

    public function getGlassesOption(): ?string
    {
        return $this->glassesOption;
    }

    public function setGlassesOption(?string $glassesOption): self
    {
        $this->glassesOption = $glassesOption;

        return $this;
    }

    /**
     * @return Collection|ProductPicture[]
     */
    public function getPicture(): Collection
    {
        return $this->picture;
    }

    public function addPicture(ProductPicture $picture): self
    {
        if (!$this->picture->contains($picture)) {
            $this->picture[] = $picture;
            $picture->setProduct($this);
        }

        return $this;
    }

    public function removePicture(ProductPicture $picture): self
    {
        if ($this->picture->contains($picture)) {
            $this->picture->removeElement($picture);
            // set the owning side to null (unless already changed)
            if ($picture->getProduct() === $this) {
                $picture->setProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SiteProductPage[]
     */
    public function getSiteProductPages(): Collection
    {
        return $this->siteProductPages;
    }

    public function addSiteProductPage(SiteProductPage $siteProductPage): self
    {
        if (!$this->siteProductPages->contains($siteProductPage)) {
            $this->siteProductPages[] = $siteProductPage;
            $siteProductPage->setRelatedProduct($this);
        }

        return $this;
    }

    public function removeSiteProductPage(SiteProductPage $siteProductPage): self
    {
        if ($this->siteProductPages->contains($siteProductPage)) {
            $this->siteProductPages->removeElement($siteProductPage);
            // set the owning side to null (unless already changed)
            if ($siteProductPage->getRelatedProduct() === $this) {
                $siteProductPage->setRelatedProduct(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|PartnerProductForSale[]
     */
    public function getPartnerProductForSales(): Collection
    {
        return $this->partnerProductForSales;
    }

    public function addPartnerProductForSale(PartnerProductForSale $partnerProductForSale): self
    {
        if (!$this->partnerProductForSales->contains($partnerProductForSale)) {
            $this->partnerProductForSales[] = $partnerProductForSale;
            $partnerProductForSale->setProduct($this);
        }

        return $this;
    }

    public function removePartnerProductForSale(PartnerProductForSale $partnerProductForSale): self
    {
        if ($this->partnerProductForSales->contains($partnerProductForSale)) {
            $this->partnerProductForSales->removeElement($partnerProductForSale);
            // set the owning side to null (unless already changed)
            if ($partnerProductForSale->getProduct() === $this) {
                $partnerProductForSale->setProduct(null);
            }
        }

        return $this;
    }
}
