<?php

namespace App\Entity\Product;

use App\Entity\Competitive\SiteBrand;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\BrandRepository")
 */
class Brand
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=64)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=128, nullable=true)
     */
    private $logo;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Product\Product", mappedBy="brand")
     */
    private $products;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Competitive\SiteBrand", mappedBy="brand" ,cascade={"persist"})
     */
    private $siteBrands;

    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->siteBrands = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->title;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getLogo(): ?string
    {
        return $this->logo;
    }

    public function setLogo(?string $logo): self
    {
        $this->logo = $logo;

        return $this;
    }

    /**
     * @return Collection|Product[]
     */
    public function getProducts(): Collection
    {
        return $this->products;
    }

    public function addProduct(Product $product): self
    {
        if (!$this->products->contains($product)) {
            $this->products[] = $product;
            $product->setBrand($this);
        }

        return $this;
    }

    public function removeProduct(Product $product): self
    {
        if ($this->products->contains($product)) {
            $this->products->removeElement($product);
            // set the owning side to null (unless already changed)
            if ($product->getBrand() === $this) {
                $product->setBrand(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|SiteBrand[]
     */
    public function getSiteBrands(): Collection
    {
        return $this->siteBrands;
    }

    public function addSiteBrand(SiteBrand $siteBrand): self
    {
        if (!$this->siteBrands->contains($siteBrand)) {
            $this->siteBrands[] = $siteBrand;
            $siteBrand->setBrand($this);
        }

        return $this;
    }

    public function removeSiteBrand(SiteBrand $siteBrand): self
    {
        if ($this->siteBrands->contains($siteBrand)) {
            $this->siteBrands->removeElement($siteBrand);
            // set the owning side to null (unless already changed)
            if ($siteBrand->getBrand() === $this) {
                $siteBrand->setBrand(null);
            }
        }

        return $this;
    }
}
