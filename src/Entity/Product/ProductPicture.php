<?php

namespace App\Entity\Product;

use App\Entity\Competitive\SiteProductPage;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Product\ProductPictureRepository")
 */
class ProductPicture
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $filename;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Competitive\SiteProductPage", inversedBy="productPictures")
     */
    private $competitiveProduct;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Product\Product", inversedBy="picture")
     */
    private $product;

    public function __toString()
    {
        return $this->filename;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFilename(): ?string
    {
        return $this->filename;
    }

    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }

    public function getCompetitiveProduct(): ?SiteProductPage
    {
        return $this->competitiveProduct;
    }

    public function setCompetitiveProduct(?SiteProductPage $competitiveProduct): self
    {
        $this->competitiveProduct = $competitiveProduct;

        return $this;
    }

    public function getProduct(): ?Product
    {
        return $this->product;
    }

    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }
}
