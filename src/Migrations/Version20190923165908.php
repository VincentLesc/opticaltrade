<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190923165908 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE product_picture (id INT AUTO_INCREMENT NOT NULL, competitive_product_id INT DEFAULT NULL, product_id INT DEFAULT NULL, filename VARCHAR(255) NOT NULL, INDEX IDX_C7025439A4938E5E (competitive_product_id), INDEX IDX_C70254394584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product_picture ADD CONSTRAINT FK_C7025439A4938E5E FOREIGN KEY (competitive_product_id) REFERENCES site_product_page (id)');
        $this->addSql('ALTER TABLE product_picture ADD CONSTRAINT FK_C70254394584665A FOREIGN KEY (product_id) REFERENCES product (id)');
        $this->addSql('ALTER TABLE product ADD prices LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:array)\', ADD size_a VARCHAR(8) NOT NULL, ADD size_d VARCHAR(8) NOT NULL, ADD size_temple VARCHAR(8) NOT NULL, ADD frame_color VARCHAR(32) NOT NULL, ADD glasses_color VARCHAR(32) NOT NULL, ADD glasses_option VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE site_product_page ADD related_product_id INT DEFAULT NULL, ADD price VARCHAR(8) DEFAULT NULL, ADD price_currency VARCHAR(8) DEFAULT NULL, ADD name VARCHAR(32) DEFAULT NULL, ADD size_a VARCHAR(8) DEFAULT NULL, ADD size_d VARCHAR(8) DEFAULT NULL, ADD size_temple VARCHAR(8) DEFAULT NULL, ADD glasses_color VARCHAR(64) DEFAULT NULL, ADD frame_color VARCHAR(32) DEFAULT NULL, ADD glasses_option VARCHAR(64) DEFAULT NULL');
        $this->addSql('ALTER TABLE site_product_page ADD CONSTRAINT FK_52680932CF496EEA FOREIGN KEY (related_product_id) REFERENCES product (id)');
        $this->addSql('CREATE INDEX IDX_52680932CF496EEA ON site_product_page (related_product_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE product_picture');
        $this->addSql('ALTER TABLE product DROP prices, DROP size_a, DROP size_d, DROP size_temple, DROP frame_color, DROP glasses_color, DROP glasses_option');
        $this->addSql('ALTER TABLE site_product_page DROP FOREIGN KEY FK_52680932CF496EEA');
        $this->addSql('DROP INDEX IDX_52680932CF496EEA ON site_product_page');
        $this->addSql('ALTER TABLE site_product_page DROP related_product_id, DROP price, DROP price_currency, DROP name, DROP size_a, DROP size_d, DROP size_temple, DROP glasses_color, DROP frame_color, DROP glasses_option');
    }
}
