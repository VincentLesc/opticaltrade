<?php

namespace App\Controller\Shop;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CartController extends AbstractController
{
    /**
     * @Route("/shop/cart", name="cart")
     */
    public function show()
    {
        return $this->render('shop/cart/cart.html.twig', [
            'controller_name' => 'CartController',
        ]);
    }
}
