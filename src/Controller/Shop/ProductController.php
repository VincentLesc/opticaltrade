<?php

namespace App\Controller\Shop;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ProductController extends AbstractController
{
    /**
     * @Route("/shop/product", name="shop_product_index")
     */
    public function index()
    {
        return $this->render('shop/product/index.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }

    /**
     * @Route("/shop/product/show", name="shop_product_show")
     */
    public function show()
    {
        return $this->render('shop/product/show.html.twig', [
            'controller_name' => 'ProductController',
        ]);
    }
}
