<?php

namespace App\Controller\Partner\Api;


use App\Entity\User;
use App\Event\PartnerEvent\EmployeeCreatedEvent;
use App\Form\EmployeeFullType;
use App\Form\EmployeeType;
use App\Repository\UserRepository;
use App\Service\User\Employee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class AccountAdminApi extends AbstractController
{
    //ToDO = Add flashes messages
    /**
     * @Route("/partner/account/employee/add/api", name="partner_add_new_employee")
     */
    public function createUserEmployee(Request $request, UserRepository $repository, Employee $employeeService, EventDispatcherInterface $dispatcher)
    {
        $data = json_decode($request->getContent());

        /**@var User $user*/
        $user = $this->getUser();

        $other = $repository->findOneBy(['email'=> $data->email]);
        if ($other) {
            return new JsonResponse([
                'type' => 'validation_error',
                'title' => 'This email is already in used'
            ], 400);
        }

        array_push($data->roles, 'ROLE_COMPANY');
        $newEmployee = $employeeService->createEmployee($data->email, $user->getPartnerCompany());
        $newEmployee->setRoles($data->roles);

        $em = $this->getDoctrine()->getManager();
        $em->persist($newEmployee);
        $em->flush();

        $event = new EmployeeCreatedEvent($newEmployee);
        $dispatcher->dispatch($event, EmployeeCreatedEvent::EMPLOYEE_CREATED);



        return new JsonResponse('done');

    }

    /**
     * @Route("/partner/account/employee/form/api", name="partner_form_employee")
     * @param Request $request
     * @param UserRepository $repository
     * @return Response
     */
    public function getFormUserEmployee(Request $request, UserRepository $repository)
    {
        $data = json_decode($request->getContent());
        if (isset($data->uid)) {
            $userId = $data->uid;
            $user = $repository->find($userId);
        } else {
            $user = new User();
        }
        $form = $this->createForm(EmployeeType::class, $user);
        return $this->render(
            'partner/account/employee_form_modal.html.twig',
            [
                'form' => $form->createView(),
                'user' => $user
            ]

        );
    }

    /**
     * @Route("/partner/account/employee/edit/api", name="partner_edit_employee")
     * @IsGranted("ROLE_COMPANY")
     * @param Request $request
     * @param UserRepository $repository
     * @param \Swift_Mailer $mailer
     * @return Response
     */
    public function editUserEmployee(Request $request, UserRepository $repository, \Swift_Mailer $mailer)
    {
        $data = json_decode($request->getContent());
        $employeeId = $data->userId;
        $employee = $repository->find($employeeId);

        /**@var User $user*/
        $user = $this->getUser();
        if ($user->getPartnerCompany() !== $employee->getPartnerCompany()) {
            return new JsonResponse('Unauthorized', 500);
        }

        $other = $repository->findOneBy(['email'=> $data->email]);
        if ($other  && $other->getId() !== (int)$employeeId) {
            return new JsonResponse([
                'type' => 'validation_error',
                'title' => 'This email is already in used'
            ], 400);
        }

        array_push($data->roles, 'ROLE_COMPANY');

        $employee->setEmail($data->email)
            ->setRoles($data->roles);

        $em = $this->getDoctrine()->getManager();
        $em->persist($employee);
        $em->flush();

        $this->addFlash('success', 'User updated');




        return new JsonResponse('done');
    }

    /**
     * @Route("/partner/account/employee/disable", name="partner_disable_employee")
     */
    public function disableUser(Request $request, UserRepository $repository)
    {
        $data = json_decode($request->getContent());
        $employeeId = $data->uid;
        $employee = $repository->find($employeeId);

        /**@var User $user*/
        $user = $this->getUser();
        if ($user->getPartnerCompany() !== $employee->getPartnerCompany()) {
            return new JsonResponse('Unauthorized', 500);
        }

        if (in_array("PARTNER_ADMIN", $employee->getRoles())) {
            return new JsonResponse('Main account can not be disactivate');
        }

        if ($employee->getIsActiveEmployee() !== true) {
            $employee->setIsActiveEmployee(true);
        } else {
            $employee->setIsActiveEmployee(false);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($employee);
        $em->flush();
        return new JsonResponse('done');
    }
}
