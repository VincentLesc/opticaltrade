<?php

namespace App\Controller\Partner;

use App\Entity\User;
use App\Form\EmployeeType;
use App\Form\RegistrationFormType;
use App\Service\User\Employee;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * Class AccountController
 * @package App\Controller\Partner
 * @isGranted("ROLE_COMPANY")
 */

class AccountController extends AbstractController
{
    /**
     * @Route("/partner/home", name="partner_blog_tutorial")
     */
    public function tutorial()
    {
        /** @var User $user */
        $user = $this->getUser();
        $company = $this->getUser()->getPartnerCompany();

        return $this->render('partner/blog/tutorial.html.twig', [
            'controller_name' => 'BlogController',
            'company' => $company
        ]);
    }

    /**
     * @Route("/partner/account", name="partner_account")
     * @param Request $request
     * @param Employee $employeeService
     * @return Response
     */
    public function index(Request $request, Employee $employeeService)
    {
        /**@var User $user **/
        $user = $this->getUser();
        $company = $user->getPartnerCompany();

        $employees = $company->getUser();

        //TODO= Add filter Twig for roles

        return $this->render('partner/account/index.html.twig', [
            'controller_name' => 'AccountController',
            'company' => $company,
            'employees' => $employees,
        ]);
    }

    /**
     * @Route("/partner/account/option", name="partner_account_option")
     */
    public function accountOption()
    {
        /**@var User $user **/
        $user = $this->getUser();
        $company = $user->getPartnerCompany();

        return $this->render('partner/option/account_option.html.twig', [
            'controller_name' => 'AccountController',
            'company' => $company,
        ]);
    }
}
