<?php

namespace App\Controller\Partner;

use App\Entity\Partner\PartnerProductForSale;
use App\Entity\User;
use App\Form\Partner\PartnerProductForSaleType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ProductForSaleController extends AbstractController
{
    /**
     * @Route("/partner/product/for/sale", name="partner_product_for_sale")
     */
    public function index()
    {
        /** @var User $user */
        $user = $this->getUser();
        $company = $user->getPartnerCompany();
        $productForSale = $company->getPartnerProductForSales();

        return $this->render('partner/product_for_sale/index.html.twig', [
            'productForSale' => $productForSale
        ]);
    }

    /**
     * @Route("/partner/product/for/sale/add", name="partner_product_for_sale")
     */
    public function add(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();
        $company = $user->getPartnerCompany();
        $productForSale = new PartnerProductForSale();
        $form = $this->createForm(PartnerProductForSaleType::class, $productForSale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $productForSale->setPartnerCompany($company);
            $em = $this->getDoctrine()->getManager();
            $em->persist($productForSale);
            $em->flush();
        }

        return $this->render('partner/product_for_sale/add.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
