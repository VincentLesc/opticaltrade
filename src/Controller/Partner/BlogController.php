<?php

namespace App\Controller\Partner;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

class BlogController extends AbstractController
{
    /**
     * @Route("/partner/blog", name="partner_blog")
     * @IsGranted("ROLE_COMPANY")
     */
    public function index()
    {
        return $this->render('partner/blog/index.html.twig', [
            'controller_name' => 'BlogController',
        ]);
    }
}
