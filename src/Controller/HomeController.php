<?php

namespace App\Controller;

use App\Repository\Content\Component\SliderRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(SliderRepository $sliderRepository)
    {
        $slides = $sliderRepository->findActiveSlidePerPage('home');

        return $this->render('home/index.html.twig', [
            'slides' => $slides
        ]);
    }
}
