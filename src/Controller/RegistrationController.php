<?php

namespace App\Controller;

use App\Entity\Partner\PartnerCompany;
use App\Entity\User;
use App\Event\PartnerEvent\PartnerCreatedEvent;
use App\Form\PartnerType;
use App\Form\RegistrationFormType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class RegistrationController extends AbstractController
{

    private $eventDispatcher;

    public function __construct(EventDispatcherInterface $eventDispatcher)
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @Route("/register", name="app_register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = new User();
        if ($request->query->get('type') === 'partner') {
            $form = $this->createForm(RegistrationFormType::class, $user, ['type' => 'partner']);
            $user->setRoles(["ROLE_COMPANY"]);
        } else {
            $form = $this->createForm(RegistrationFormType::class, $user);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // encode the plain password
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            );

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            // do anything else you need here, like send an email
            if ($request->query->get('type') === 'partner') {
                $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
                $this->container->get('security.token_storage')->setToken($token);
                $this->container->get('session')->set('_security_main', serialize($token));
                return $this->redirectToRoute('partner_register_company');
            } else {
                return $this->redirectToRoute('');
            }
        }

        return $this->render('registration/register.html.twig', [
            'registrationForm' => $form->createView(),
        ]);
    }

    /**
     * @Route("/register/partner/company", name="partner_register_company")
     * @IsGranted("ROLE_COMPANY")
     */
    public function registerCompany(Request $request): Response
    {

        //TODO = Add security on forms
        //TODO = Add adresses api for autocomplete
        //TODO = SIRET and other legal number to string type with regex
        //TODO = Send mail to confirm company registration

        $company = new PartnerCompany();
        /** @var User $user */
        $user = $this->getUser();
        $company->addUser($user);
        $company->setMainUser($user);
        $user->setRoles(["PARTNER_ADMIN"]);
        $user->setIsActiveEmployee(true);

        $form = $this->createForm(PartnerType::class, $company);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($company);
            $entityManager->flush();
            $event = new PartnerCreatedEvent($company);
            $this->eventDispatcher->dispatch($event, PartnerCreatedEvent::PARTNER_CREATED);

            return $this->redirectToRoute('partner_blog_tutorial');
        }

        return $this->render('registration/registerPartner.html.twig',[
            'form' => $form->createView()
        ]);
    }
}
