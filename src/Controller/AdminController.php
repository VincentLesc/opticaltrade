<?php

namespace App\Controller;

use App\Entity\Competitive\Site;
use App\Entity\Competitive\SiteBrand;
use App\Entity\Competitive\SiteProductPage;
use App\Repository\Competitive\SiteBrandRepository;
use App\Repository\Partner\PartnerCompanyRepository;
use App\Service\Admin\CompetitiveScan;
use Symfony\Component\Routing\Annotation\Route;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AdminController as BaseAdminController;

class AdminController extends BaseAdminController
{

    private $scan;

    public function __construct(CompetitiveScan $scan)
    {
        $this->scan = $scan;
    }

    /**
     * @Route("/dashboard", name="admin_dashboard")
     */
    public function dashboard(PartnerCompanyRepository $companyRepository)
    {
        $partnerAccountToBeValidated = count($companyRepository->findBy(['isConfirm' => false], ['id'=> 'DESC']));
        return $this->render('app/admin/partner/dashboard.html.twig',[
            'companyToBeConfirmedCount' => $partnerAccountToBeValidated
        ]);
    }

    public function parseIndexBrandPageAction()
    {
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository(SiteBrand::class)->find($id);
        $url = $entity->getBaseUrl();
        $crawler = $this->scan->crawlerBrandIndex($url, $entity);


        return $this->redirectToRoute('easyadmin', [
            'action' => 'list',
            'entity' => $this->request->query->get('entity')
        ]);
    }

    public function parseProductPageAction()
    {
        $id = $this->request->query->get('id');
        $entity = $this->em->getRepository(SiteProductPage::class)->find($id);
        $url = $entity->getSiteBrand()->getSite()->getBaseUrl() . $entity->getUrl();
        $entity = $this->scan->crawlerProductPage($url, $entity);

        dump($entity);

        return $this->redirectToRoute('easyadmin', [
            'action' => 'edit',
            'id' => $id,
            'entity' => $this->request->query->get('entity')
        ]);
    }

    public function addProductAction()
    {
        $id = $this->request->query->get('id');
        $originItem = $this->em->getRepository(SiteProductPage::class)->find($id);
        $product = $this->scan->transferToProduct($originItem);

        return $this->redirectToRoute('easyadmin', [
            'action' => 'list',
            'entity' => $this->request->query->get('entity')
        ]);

    }
}
