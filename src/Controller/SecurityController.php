<?php

namespace App\Controller;

use App\Form\PasswordTokenType;
use App\Form\Security\PasswordResetType;
use App\Repository\UserRepository;
use App\Service\User\UserSecurity;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     * @param AuthenticationUtils $authenticationUtils
     * @return Response
     */
    public function login(AuthenticationUtils $authenticationUtils, Request $request): Response
    {
         if ($this->getUser() && in_array('ROLE_COMPANY', $this->getUser()->getRoles())) {
            return $this->redirectToRoute('partner_blog_tutorial');
         }
        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        if ($request->query->get('type') === 'partner') {
            return $this->render('security/partner_login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
        } else {
            return $this->render('security/login.html.twig', ['last_username' => $lastUsername, 'error' => $error]);
        }
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new Exception('This method can be blank - it will be intercepted by the logout key on your firewall');
    }

    /**
     * @Route("/password/set/{token}", name="app_password_set")
     * @param string $token
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserPasswordEncoderInterface $passwordEncoder
     * @return Response
     * @throws Exception
     */
    public function updatePasswordAfterToken(string $token, Request $request, UserRepository $userRepository, UserPasswordEncoderInterface $passwordEncoder): Response
    {
        $user = $userRepository->findOneBy(['passwordToken'=>$token]);

        $error = [];
        if (!$user) {
            $error = ['noUser'];
            return $this->render('security/password.html.twig', [
                'error' => $error
            ]);
        }

        $interval = date_diff($user->getPasswordTokenCreatedAt(), new \DateTime('now'));
        if ($interval->days >= $this->getParameter('app.security.token.validity')) {
            $error = ['expiredToken'];
            return $this->render('security/password.html.twig', [
                'error' => $error,
                'user'=> $user
            ]);
        }

        $form = $this->createForm(\App\Form\Security\PasswordTokenType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword(
                $passwordEncoder->encodePassword(
                    $user,
                    $form->get('plainPassword')->getData()
                )
            )
                ->setPasswordToken(null)
                ->setPasswordTokenCreatedAt(null);

            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/password.html.twig', [
            'form' => $form->createView(),
            'error' => $error,
            'user' => $user
        ]);
    }

    /**
     * @Route("/password/reset", name="app_password_ask_reset")
     * @param Request $request
     * @param UserRepository $userRepository
     * @param UserSecurity $security
     * @throws Exception
     */
    public function askPasswordReset(Request $request, UserRepository $userRepository, UserSecurity $security)
    {
        $form = $this->createForm(PasswordResetType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $email = $form->get('email')->getData();
            $user = $userRepository->findOneBy(['email'=> $email]);
            if (!$user) {
                //TODO = Add view error
                var_dump('no_user');
                die();
            }
            $user->setPasswordToken($security->getRandomToken())
                ->setPasswordTokenCreatedAt(new \DateTime('now'));

            $em = $this->getDoctrine()->getManager();
            $em->flush();

            $security->sendMailWithPasswordResetToken($user);
            //TODO = Add Flashes message on login
            //TODO = Redirect on origin ask page
            return $this->redirectToRoute('app_login');
        }

        return $this->render('security/password_reset.html.twig', [
                'form' => $form->createView()
            ]
        );
    }
}
